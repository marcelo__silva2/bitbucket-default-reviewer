## Installation
Install the dependencies and script run example

```sh
pip install -r requirements.txt
```

```sh
export BITBUCKET_USER="USER"
export BITBUCKET_PASSWORD="PASS"
python bitbucket.py -a ACTION -p FILE -u USERS -w WORKSPACE
```

ACTION: add/remove
USERS: user1,user2,...
WORKSPACE: Workspace slug
FILE: File with name of the repos one per line
