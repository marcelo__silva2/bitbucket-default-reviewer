from atlassian.bitbucket import Cloud
from requests import HTTPError
import argparse
import os

my_parser = argparse.ArgumentParser()
my_parser.add_argument('-a', '--action', action='store', type=str, required=True)
my_parser.add_argument('-p', '--projectsFile', action='store', type=str, required=True)
my_parser.add_argument('-u', '--users', action='store', type=str, required=True)
my_parser.add_argument('-w', '--workspace', action='store', type=str, required=True)
args = my_parser.parse_args()

USER = os.getenv('BITBUCKET_USER')
PASSWORD = os.getenv('BITBUCKET_PASSWORD')

bitbucket = Cloud(
    url='https://api.bitbucket.org/',
    username=USER,
    password=PASSWORD,
    cloud=True)


def get_repository_names(file):
    try:
        repositories = open(file).read().split('\n')
        if len(repositories) <= 0:
            print('Empty file')
            exit()
        return repositories
    except:
        print('Invalid file')
        exit()


def get_repository(workspace, repository_name):
    try:
        return bitbucket.workspaces.get(workspace).repositories.get(repository_name)
    except:
        print("Failed to get " + repository_name)


def main():
    action = args.action
    users = args.users.split(",")
    workspace = args.workspace
    if action != 'add' and action != 'remove':
        print('Invalid action: ' + action)
        exit()

    repositories = get_repository_names(args.projectsFile)

    for repository in repositories:
        repo = get_repository(workspace, repository)
        added_users = ''
        failed_users = ''
        for user in users:
            data = '{mention_id: "' + user + '"}'
            try:
                if args.action == 'add':
                    repo.default_reviewers.put(path=user, data=data)
                elif args.action == 'remove':
                    repo.default_reviewers.delete(path=user)
                added_users = added_users.join(user + ', ')
            except HTTPError as e:
                failed_users = failed_users.join(user + ', ')
        print(
            'Finished repository ' + repo.name + ' \nUsers success: ' + added_users + ' \nUsers failed: ' + failed_users + ' \n\n')


main()
